# S

硫-S，位于周期表第三周期，第IVA族的非金属，常见价态有：-2、0、+4、+6。在自然界中以硫黄等形式存在游离态。

可用于生产硫酸、硫肥、农药、火柴、含硫橡胶、肥皂等

---

### S单质

俗称硫黄

是一种黄色晶体，质脆，易研成粉末。

难溶于水，微溶于酒精，易溶于$\ce{CS2、CCl4}$等非极性溶剂

存在多种同素异形体（$\ce{S2、S4、S6、S8}$等），$\ce{S8}$最稳定

具有氧化性与还原性（氧化性<$\ce{Cl2}$）

* 以$\ce{FeS2}$制取硫单质:

$\ce{3FeS2 + 12C + 8O2 \xlongequal{点燃} Fe3O4 + 12 CO + 6S}$

* 硫可与除$\ce{Au、Pt}$外的几乎所有金属在加热条件下直接化合;
  
  $\ce{Hg + S \xlongequal{(\Delta)} HgS}$

——$\ce{HgS}$：朱砂

- 只能将变价金属氧化到低价。
  
  $\ce{Fe + S \xlongequal{\Delta} FeS}$
  
  $\ce{2Cu + S \xlongequal{\Delta} Cu2S}$
* 硫一般可以与除稀有气体，碘、氮以外的非金属单质直接化合
  
  $\ce{O2 + S \xlongequal{点燃} SO2}$
  
  $\ce{C + 2S \xlongequal{\Delta} CS2}$
  
  $\ce{H2 + S \xlongequal{\Delta} H2S}$

* 硫可被氧化性酸氧化
  
  $\ce{S + 2HNO3(浓) = H2SO4 + 2NO\uparrow }$
  
  $\ce{S + 2H2SO4(浓) \xlongequal{\Delta} 3SO2\uparrow + 2NO\uparrow}$

* 硫在碱性条件下歧化到-2和+4价，加热可加快反应
  
  $\ce{3S + 6NaOH \xlongequal{\Delta} 2Na2S + Na2SO3 + 3H2O}$
  
  ^ $\ce{3S + 6OH- \xlongequal{\Delta} 2S^2- + SO3^2- + 3H2O}$

* 烟花
  
  $\ce{S + 2KNO3 + 3C \xlongequal{点燃} K2S + N2\uparrow + 3O2\uparrow}$

---

### $\ce{SO2}$

一种无色，有刺激性气味的有毒气体，酸性氧化物，易溶于水(1：40)

硫处于中间价态，同时具有氧化性和还原性

- 溶于水
  
  $\ce{SO2 + H2O <=> H2SO3}$

$\ce{2SO2 + O2 <=>[V2O5][\Delta] 2SO3}$

$\ce{SO2 + 2H2S = 3S(\downarrow) + 2H2O}$ （沉淀符号看情况）
